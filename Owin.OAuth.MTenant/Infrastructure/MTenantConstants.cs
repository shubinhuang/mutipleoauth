﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Owin.OAuth.MTenant.Infrastructure
{
    public static class MTenantConstants
    {
        public const string AuthenticationTenantKey = "tenant_key";
        public const string AuthenticationProviderKey = "provider_key";
    }
}
